<?php
class bundesgesetzblatt {
	private const URI = 'https://www.bgbl.de/xaver/bgbl/custom/mobile/content.xqy';
	private const POSTFIELDS = 'data-title=&data-mode=current';

	public function query() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::URI);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, self::POSTFIELDS);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$json = curl_exec($ch);
		curl_close($ch);
		return json_decode($json, TRUE);
	}
}

class alexa {
	private const DISPLAY_URI = 'https://www.bgbl.de/xaver/bgbl/start.xav?startSkin=mobile';

	public function generateFlashBriefing(array $bgbl,
					      bool $content = FALSE) {
		$date = substr($bgbl['title'], -10);
		$fb['updateDate'] = substr($date, -4) . '-'
				  . substr($date, -7, 2) . '-'
				  . substr($date, 0, 2) . 'T'
				  . '02:00:00.0Z';
		$fb['titleText'] = $bgbl['title'];
		$fb['redirectionUrl'] = self::DISPLAY_URI;
		$fb['mainText'] = substr($bgbl['title'], 27) . '. ';
		if ($content) {
			foreach($bgbl['toc']['list']['item'] as $item) {
				if (   $item['title'] != 'Komplette Ausgabe'
				    && $item['title'] != 'Inhaltsverzeichnis'
				    && strncmp($item['title'],
					       'Hinweis: ', 9) !== 0
				   ) {
					$fb['uid'] .= $item['tocNodeId'] . '-';
					$fb['mainText'] .= $item['title']
							. '. ';
				}
			}
		}
		if (!isset($fb['uid']) || empty($fb['uid'])) {
			$fb['uid'] = uniqid();
		}
		return json_encode($fb, JSON_UNESCAPED_SLASHES
				      | JSON_UNESCAPED_UNICODE);
	}
}

$bgbl = new bundesgesetzblatt;
$flashBriefing = new alexa;

header('Content-Type: application/json');
echo $flashBriefing->generateFlashBriefing($bgbl->query(),
				$_GET['length'] === 'full' ? TRUE : FALSE);
unset($bgbl);
unset($flashBriefing);
